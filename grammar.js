const PREC = {
  brackets: 16,
  call: 15,
  field: 14,
  try: 13,
  unary: 12,
  cast: 11,
  multiplicative: 10,
  additive: 9,
  shift: 8,
  bitand: 7,
  bitxor: 6,
  bitor: 5,
  comparative: 4,
  and: 3,
  or: 2,
  range: 1,
  assign: 0,
  closure: -1,
  syntax_sugar: -99,
};

module.exports = grammar({
  name: "ignis",

  extras: ($) => [/\s/, $.line_comment],

  rules: {
    source_file: ($) => repeat($._top_level_statement),

    line_comment: (_) => token(seq("//", /.*/)),

    unary_expression: ($) =>
      prec(PREC.unary, seq(choice("-", "!"), $._expression)),

    binary_expression: ($) => {
      const table = [
        [PREC.and, "&&"],
        [PREC.or, "||"],
        [PREC.bitand, "&"],
        [PREC.bitor, "|"],
        [PREC.bitxor, "^"],
        [PREC.comparative, choice("==", "!=", "<", "<=", ">", ">=")],
        [PREC.shift, choice("<<", ">>")],
        [PREC.additive, choice("+", "-", "+.", "-.", "++")],
        [PREC.multiplicative, choice("*", "/", "*.", "/.", "%")],
        [PREC.cast, choice("??")],
      ];

      // @ts-ignore
      return choice(
        ...table.map(([precedence, operator]) =>
          prec.left(
            precedence,
            seq(
              field("left", $._expression),
              // @ts-ignore
              field("operator", operator),
              field("right", $._expression),
            ),
          ),
        ),
      );
    },

    compound_assignment_expression: ($) =>
      prec.left(
        PREC.assign,
        seq(
          field("left", choice($._ident, $.prop_expr, $.index_expr)),
          field(
            "operator",
            choice(
              "+=",
              "-=",
              "*=",
              "/=",
              "%=",
              "&=",
              "|=",
              "^=",
              "<<=",
              ">>=",
            ),
          ),
          field("right", $._expression),
        ),
      ),

    assignment_stmt: ($) =>
      prec.left(
        PREC.assign,
        seq(
          choice($._ident, $.prop_expr, $.index_expr),
          "=",
          $._expression,
          choice(";", $._newline),
        ),
      ),

    index_expr: ($) =>
      prec(PREC.call, seq($._expression, "[", $._expression, "]")),

    arguments: ($) =>
      seq(choice(".(", "("), sepBy(",", $._expression), optional(","), ")"),

    call_expression: ($) =>
      prec(
        PREC.call,
        seq(field("function", $._expression), field("arguments", $.arguments)),
      ),

    tuple_destruct: ($) => seq("(", sepBy(",", $._ident), ")"),
    array_destruct: ($) => seq("[", sepBy(",", $._ident), "]"),
    record_destruct: ($) => seq("{", sepBy(",", $._ident), "}"),

    _destruct: ($) =>
      choice($._ident, $.tuple_destruct, $.array_destruct, $.record_destruct),

    var_definition: ($) =>
      seq(
        $.let,
        optional($.mut),
        $._destruct,
        optional(seq(":", $._typ)),
        choice("=", "$="),
        $._expression,
        $._line_term,
      ),

    anon_fndef: ($) =>
      seq(
        $.fn,
        optional($.type_arguments),
        $.parameter_list,
        optional(seq(optional("->"), $._typ)),
        $.block,
      ),

    fn_name: ($) => $._ident,

    named_fndef: ($) =>
      seq(
        $.fn,
        field("fn_name", $.fn_name),
        optional($.type_arguments),
        $.parameter_list,
        optional(seq(optional("->"), $._typ)),
        $.block,
      ),

    typ_params: ($) => seq("<", sepBy1(",", $._typ), ">"),

    typ_tuple: ($) => seq("(", sepBy1(",", $._typ), ")"),
    typ_fn: ($) =>
      seq(
        $.fn,
        optional($.typ_params),
        $.parameter_list,
        optional("->"),
        $._typ,
      ),
    typ: ($) => seq($.mod_ident, optional($.typ_params)),

    _typ: ($) => choice($.typ, $.typ_fn, $.typ_tuple),

    _param_typ: ($) => seq(":", $._typ),
    _param_default: ($) => seq("=", choice("?", $._non_block_expression)),

    parameter: ($) =>
      choice(
        $.self,
        seq(
          $._ident,
          optional(
            choice(
              seq($._param_typ, optional($._param_default)),
              seq($._param_default, optional($._param_typ)),
            ),
          ),
        ),
        "_",
      ),

    parameter_list: ($) =>
      seq("(", sepBy(",", $.parameter), optional(","), ")"),

    block: ($) => seq("{", repeat($._block_statement), "}"),

    comp_statement: ($) => seq($.comp, optional($.parameter_list), $.block),
    mod_statement: ($) =>
      seq($.mod, $.mod_ident, "{", repeat($._top_level_statement), "}"),

    pragma: ($) =>
      seq(
        $.pragma_literal,
        optional(choice(seq("=", $._ident), seq($._ident, ":", $._typ))),
        $._line_term,
      ),

    break: ($) => seq($.break, $._line_term),
    continue: ($) => seq($.continue, $._line_term),

    type_arguments: ($) =>
      seq(token("<"), sepBy1(",", $.mod_ident), optional(","), ">"),

    typedef_field: ($) => seq($.field_ident, ":", $._typ),

    typedef_params: ($) => seq("<", sepBy1(",", $.mod_ident), ">"),

    tag_typ_params: ($) => seq("(", sepBy1(",", $._typ), ")"),

    typedef: ($) =>
      seq(
        $.type,
        optional($.typedef_params),
        choice(
          seq("{", sepBy(",", $.typedef_field), optional(","), "}"),
          repeat(seq("|", $.tag_ident, optional($.tag_typ_params))),
        ),
      ),

    loop: ($) => seq($.loop, $.block),

    while: ($) => seq($.while, $._expression, $.block),

    for_loop: ($) => seq($.for, $._destruct, $.in, $._expression, $.block),

    _statement: ($) => choice($.var_definition, $.loop, $.while, $.for_loop),

    _block_statement: ($) =>
      choice(
        $._statement,
        seq($._expression, $._line_term),
        $.break,
        $.continue,
        $.return_statement,
        $.assignment_stmt,
      ),

    _top_level_statement: ($) =>
      choice(
        $.pragma,
        $.comp_statement,
        $.mod_statement,
        $._statement,
        $.typedef,
        $.named_fndef,
      ),

    return_statement: ($) => seq($.return, $._expression, $._line_term),

    else_clause: ($) => seq($.else, choice($.block, $.if_expression)),

    if_expression: ($) =>
      prec.right(
        seq(
          $.if,
          field("condition", $._expression),
          field("consequence", $.block),
          optional(field("alternative", $.else_clause)),
        ),
      ),

    parenthesized_expression: ($) =>
      prec(PREC.brackets, seq("(", $._expression, ")")),

    // Required special case to take precedence over float
    range_literal: (_$) => /\d[\d_]*\.\.=?\d[\d_]*/,
    range_expression: ($) =>
      choice(
        $.range_literal,
        prec.left(
          PREC.range,
          seq($._expression, choice("..", "..="), $._expression),
        ),
      ),

    _jsx_prop_value: ($) =>
      choice(
        seq("{", $._expression, "}"),
        $.string_literal,
        $.bool_literal,
        $._number_literal,
        $._var,
        /@[a-zA-Z][_a-zA-Z]/,
      ),

    jsx_prop_ident: (_$) => choice(/@?[a-zA-Z][_a-zA-Z0-9\-]*/),

    jsx_prop: ($) =>
      choice(
        seq("{", optional("@"), $._ident, optional("()"), "}"),
        seq($.jsx_prop_ident, optional(seq("=", $._jsx_prop_value))),
      ),

    jsx_opening_element: ($) =>
      choice(seq("<", $._jsx_element_ident, repeat($.jsx_prop), ">"), "<>"),
    jsx_closing_element: ($) =>
      choice(seq("</", $._jsx_element_ident, ">"), "</>"),

    jsx_tag: ($) =>
      seq($.jsx_opening_element, repeat($._jsx_expr), $.jsx_closing_element),
    jsx_self_close_tag: ($) =>
      seq("<", $._jsx_element_ident, repeat($.jsx_prop), "/>"),
    jsx_inner_expr: ($) => seq("{", $._expression, "}"),

    _jsx_expr: ($) => choice($.jsx_tag, $.jsx_self_close_tag, $.jsx_inner_expr),

    _jsx_expr_start: ($) => choice($.jsx_tag, $.jsx_self_close_tag),

    record_spread: ($) => seq("...", $._ident),
    field: ($) => seq($._ident, ":", $._expression),
    field_pun: ($) => $._ident,

    record: ($) =>
      seq(
        choice(seq($.mod_ident, "{"), "#{"),
        sepBy(",", choice($.record_spread, $.field, $.field_pun)),
        optional(","),
        "}",
      ),

    tuple_expression: ($) => seq("(", sepBy(",", $._expression), ")"),

    array_expression: ($) => seq("[", sepBy(",", $._expression), "]"),

    _tag_name: ($) => choice($.tag_ident, seq($.mod_ident, $.tag_ident)),

    tag: ($) =>
      choice(
        // Plain mod_ident gets reformatted to tag for convenience
        prec.left(PREC.syntax_sugar, $.mod_ident),
        $._tag_name,
        prec(2, seq($._tag_name, "(", sepBy1(",", $._expression), ")")),
      ),

    tag_destruct: ($) =>
      choice(
        // Plain mod_ident gets reformatted to tag for convenience
        $.mod_ident,
        $._tag_name,
        prec(2, seq($._tag_name, "(", sepBy1(",", $._destruct), ")")),
      ),

    mod_var: ($) => prec.left(seq($.mod_ident, ".", sepBy1(".", $._ident))),
    _var: ($) => choice($._ident, $.tag, $.self, $.mod_var),

    prop_expr: ($) =>
      seq(
        $._val_expression,
        // optional($._newline),
        ".",
        field("property", $._ident),
      ),

    tuple_destruct: ($) => seq("(", sepBy(",", $._destruct), ")"),

    _match_branch_destruct: ($) =>
      choice(
        $._destruct,
        $.tag_destruct,
        $.string_literal,
        $._number_literal,
        $.bool_literal,
      ),

    match_branch: ($) =>
      seq(
        $._match_branch_destruct,
        "=>",
        choice(
          seq($._non_block_expression, choice(",", ";", $._newline)),
          seq($.block, choice(",", $._newline)),
        ),
      ),

    match_exp: ($) =>
      seq($.match, $._expression, "{", repeat($.match_branch), "}"),

    _literal_expression: ($) =>
      choice(
        $._number_literal,
        $.bool_literal,
        $.string_literal,
        $.regex_literal,
        $.range_expression,
        // prec.right(99, $.range_expression),
      ),

    // Expressions on which you can call `expr.prop`
    _val_expression: ($) =>
      choice(
        $._var,
        $.prop_expr,
        $.index_expr,
        $.call_expression,
        $.parenthesized_expression,
        $.array_expression,
      ),

    _non_block_expression: ($) =>
      choice(
        $._literal_expression,
        $._val_expression,
        $.anon_fndef,
        $.unary_expression,
        $.binary_expression,
        $.compound_assignment_expression,
        $.if_expression,
        $.tuple_expression,
        $._jsx_expr_start,
        $.match_exp,
        $.record,
      ),

    _expression: ($) => choice($._non_block_expression, $.block),

    _newline: (_$) => token("\n"),
    _line_term: ($) => choice($._newline, ";"),

    eta_ident: (_$) => /@[a-z][_a-z0-9]*/,
    ident: (_$) => /[a-z][_a-z0-9]*/,
    ignored_ident: (_$) => /_([a-z_0-9]*)?/,
    fn_placeholder_ident: (_$) => "$",

    _ident: ($) =>
      choice($.ident, $.ignored_ident, $.eta_ident, $.fn_placeholder_ident),

    tag_ident: (_$) => /#[a-zA-Z][_A-Za-z0-9]*/,
    mod_ident: (_$) => /@?([A-Z][A-Za-z_0-9]*\.)*([A-Z][A-Za-z_0-9]*)/,
    jsx_element_base_ident: (_$) => /[a-z][_a-zA-Z0-9]*/,
    jsx_element_mod_ident: (_$) =>
      /@?([A-Z][A-Za-z_0-9]*\.)*([A-Z][A-Za-z_0-9]*)/,
    _jsx_element_ident: ($) =>
      choice($.jsx_element_base_ident, $.jsx_element_mod_ident),
    field_ident: (_$) => /[a-z][a-z_0-9]*/,

    int_literal: (_$) => /[\d][\d_]*/,
    float_literal: (_$) => /[\d][\d_]*\.[\d_]*/,
    _number_literal: ($) => choice($.int_literal, $.float_literal),
    bool_literal: (_$) => choice("true", "false"),
    // string_literal: (_$) => /\"[^\"]*\"/,
    string_literal: (_$) => /\"([^\"\\]|\\.)*\"/,
    regex_literal: (_$) => /r\"[^\"]*\"/,
    pragma_literal: (_$) => /%%[_a-z]+/,

    let: (_) => "let",
    mut: (_) => "mut",
    fn: (_) => "fn",
    break: (_) => "break",
    continue: (_) => "continue",
    loop: (_) => "loop",
    while: (_) => "while",
    for: (_) => "for",
    return: (_) => "return",
    else: (_) => "else",
    type: (_) => choice("type", "enum", "rec"),
    match: (_) => "match",
    in: (_) => "in",
    if: (_) => "if",
    self: (_) => "self",
    comp: (_) => /comp!?/,
    mod: (_) => "mod",
  },
});

/**
 * Creates a rule to match one or more of the rules separated by the separator.
 *
 * @param {RuleOrLiteral} sep - The separator to use.
 * @param {RuleOrLiteral} rule
 *
 * @return {SeqRule}
 *
 */
function sepBy1(sep, rule) {
  return seq(rule, repeat(seq(sep, rule)));
}

/**
 * Creates a rule to optionally match one or more of the rules separated by the separator.
 *
 * @param {RuleOrLiteral} sep - The separator to use.
 * @param {RuleOrLiteral} rule
 *
 * @return {ChoiceRule}
 *
 */
function sepBy(sep, rule) {
  return optional(sepBy1(sep, rule));
}
