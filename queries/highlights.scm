(line_comment) @comment


[
  ";"
  "."
  ","
  ; "..",
] @punctuation.delimiter

[
  "-"
  "-="
  ; "-.",
  "+"
  "+="
  ; "+.",
  ; "++",
  "*"
  "*="
  ; "*.",
  "/"
  "/="
  ; "/.",
  "<"
  "<="
  "<<"
  "="
  "$="
  "=="
  "!"
  "!="
  ">"
  ">="
  ">>"
  "^"
  "&"
  "|"
  "&&"
  "||"
  "??"
] @operator

[
  "("
  ")"
  "["
  "]"
  "{"
  "}"
]  @punctuation.bracket

; (type_arguments
;   "<" @punctuation.bracket
;   ">" @punctuation.bracket)
; (type_parameters
;   "<" @punctuation.bracket
;   ">" @punctuation.bracket)

(parameter (ident) @variable.parameter)


[
  (if)
  (else)
  (match)
] @keyword.control.conditional
[
  (loop)
  (for)
  (while)
] @keyword.control.repeat
[
  (break)
  (continue)
  (return)
] @keyword.control.return
[
  (fn)
] @keyword.function
[
  (in)
] @keyword.operator
[
  (type)
  (let)
] @keyword.storage.type
[
  (mut)
] @keyword.storage.modifier


(jsx_element_base_ident) @string.tag
(jsx_element_mod_ident) @type.tag

(jsx_prop_ident) @tag
(jsx_opening_element (["<" ">"]) @punctuation.bracket)
(jsx_closing_element (["</" ">"]) @punctuation.bracket)
(jsx_self_close_tag (["<" "/>"]) @punctuation.bracket)

(self) @variable.builtin
(comp) @variable.builtin
(pragma_literal) @variable.builtin
(bool_literal) @constant.builtin
(int_literal) @constant.builtin
(float_literal) @constant.builtin
(range_literal) @constant.builtin
(string_literal) @string
(regex_literal) @string
(mod_ident) @type
(tag_ident) @type.enum.variant
(field_ident) @property
(call_expression function: (ident) @function)
(call_expression function: (prop_expr property: (ident) @function))
(ident) @variable
(eta_ident) @variable.parameter
(fn_placeholder_ident) @variable.parameter.placeholder

(fn_name (ident)) @function

; @constant.macro
; @namespace
; @parameter
; @annotation
; @conditional
; @exception
; (jsx_identifier) @tag
; (jsx_element
;   open_tag: (jsx_opening_element ["<" ">"] @tag.delimiter))
; (jsx_element
;   close_tag: (jsx_closing_element ["<" "/" ">"] @tag.delimiter))
; (jsx_self_closing_element ["/" ">" "<"] @tag.delimiter)
; (jsx_fragment [">" "<" "/"] @tag.delimiter)
; (jsx_attribute (property_identifier) @tag.attribute)

; Error
;----------

(ERROR) @error
