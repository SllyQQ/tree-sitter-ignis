; type aliases
; (typedef) @definition.class

; function definitions
; (named_fndef
;     (fn_name (ident)) @name) @definition.function
; (anon_fndef) @definition.function

; references
; (call_expression
;     function: (ident) @name) @reference.call

; (call_expression
;     function: (field_expression
;         field: (field_identifier) @name)) @reference.call
