;-------

[
  ; (statement_block)
  (named_fndef)
  (anon_fndef)
] @local.scope

; Definitions
;------------

(parameter (ident) @local.definition)

; References
;------------

(ident) @local.reference
