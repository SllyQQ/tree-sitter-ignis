==================
Jsx Expr2
==================

comp! {
  <AsoAugInfo aug_id={aug_ids[0.]} stats={@stats[aug_ids[0.]]} />;
}

---


(source_file
  (comp_statement
    (comp)
    (block
      (jsx_self_close_tag
        (jsx_element_mod_ident)
        (jsx_prop
          (jsx_prop_ident)
          (index_expr
            (ident)
            (float_literal)))
        (jsx_prop
          (jsx_prop_ident)
          (index_expr
            (eta_ident)
            (index_expr
              (ident)
              (float_literal))))))))
