==================
Jsx Stuff
==================

comp! {
   <g
      {@transform}
   >
      <rect
         {@on_click}
         cursor="pointer"
      />
   </g>
}

---

(source_file
  (comp_statement
    (comp)
    (block
      (jsx_tag
        (jsx_opening_element
          (jsx_element_base_ident)
          (jsx_prop
            (eta_ident)))
        (jsx_self_close_tag
          (jsx_element_base_ident)
          (jsx_prop
            (eta_ident))
          (jsx_prop
            (jsx_prop_ident)
            (string_literal)))
        (jsx_closing_element
          (jsx_element_base_ident))
        )
      )
    )
  )

