==================
Jsx Expr
==================

comp! {
  <div class="absolute" style={{
     left: x1 + "px",
     top: y + "px",
  }}>
    <div />
  </div>;
}

---

(source_file
  (comp_statement
    (comp)
    (block
      (jsx_tag
        (jsx_opening_element
          (jsx_element_base_ident)
          (jsx_prop
            (jsx_prop_ident)
            (string_literal))
          (jsx_prop
            (jsx_prop_ident)
            (record
              (field
                (ident)
                (binary_expression
                  (ident)
                  (string_literal)))
              (field
                (ident)
                (binary_expression
                  (ident)
                  (string_literal)))))
        )
        (jsx_self_close_tag
          (jsx_element_base_ident))
        (jsx_closing_element
          (jsx_element_base_ident))
      )
    )
  )
)
