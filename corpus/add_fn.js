==================
Add fndef
==================

let add = fn(x, y) {
  return x + y;
};

---

(source_file
  (var_definition
    (let)
    (ident)
    (anon_fndef
      (fn)
      (parameter_list
        (parameter
          (ident))
        (parameter
          (ident)))
      (block
        (return_statement
          (return)
          (binary_expression
            (ident)
            (ident)))))))