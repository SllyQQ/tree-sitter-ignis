==================
Self Method
==================

type<T>
   | #Some(T)
   | #None

fn map(self, map_fn) {
   match self {
      #Some(v) => #Some(map_fn(v)),
      #None => #None,
   };
}

---

(source_file
  (typedef
    (type)
    (typedef_params
      (mod_ident))
    (tag_ident)
    (tag_typ_params
      (typ
        (mod_ident)))
    (tag_ident))
  (named_fndef
    (fn)
    (fn_name
      (ident))
    (parameter_list
      (parameter
        (self))
      (parameter
        (ident)))
    (block
      (match_exp
        (match)
        (self)
        (match_branch
          (tag_destruct
            (tag_ident)
            (ident))
          (tag
            (tag_ident)
            (call_expression
              (ident)
              (arguments
                (ident)))))
        (match_branch
          (tag_destruct
            (tag_ident))
          (tag
            (tag_ident)))))))
