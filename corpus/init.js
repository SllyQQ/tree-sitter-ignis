==================
Init
==================

let _ = d.init()

---

(source_file
  (var_definition
    (let)
    (ignored_ident)
    (call_expression
        (prop_expr
          (ident)
          (ident))
        (arguments))))
