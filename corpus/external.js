==================
External Fn
==================

%%external create_signal: fn<T>(init: T) (fn() T, fn(new_val: T) Unit);

---

(source_file
  (pragma
    (pragma_literal)
    (ident)
    (typ_fn
      (fn)
      (typ_params
        (typ
          (mod_ident)))
      (parameter_list
        (parameter
          (ident)
          (typ
            (mod_ident))))
      (typ_tuple
        (typ_fn
          (fn)
          (parameter_list)
          (typ
            (mod_ident)))
        (typ_fn
          (fn)
          (parameter_list
            (parameter
              (ident)
              (typ
                (mod_ident))))
          (typ
            (mod_ident)))))))
