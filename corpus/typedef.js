==================
Typedef
==================

type {
  f1: String,
  f2: Int,
}

---

(source_file
  (typedef
    (type)
    (typedef_field
      (field_ident)
      (mod_ident))
    (typedef_field
      (field_ident)
      (mod_ident))
  )
)
