==================
Anon Fn Def
==================

let a = fn() {
    return 1;
};

---

(source_file
  (var_definition
    (let)
    (ident)
    (anon_fndef
      (fn)
      (parameter_list)
      (block
        (return_statement
          (return)
          (int_literal))))))
