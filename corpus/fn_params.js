==================
Fn params
==================

let qq = fn(p1, p2, p3) {
    self_fn_call(a, b, c);
};

---

(source_file
  (var_definition
    (let)
    (ident)
    (anon_fndef
      (fn)
      (parameter_list
        (parameter
          (ident))
        (parameter
          (ident))
        (parameter
          (ident)))
      (block
        (call_expression
          (ident)
          (arguments
            (ident)
            (ident)
            (ident)))
           ))))