==================
Variant
==================

let a = #B((1., 2.));

let _ = match a {
   #A => 0.,
   #B((a, b)) => a,
}

---

(source_file
  (var_definition
    (let)
    (ident)
    (tag
      (tag_ident)
      (tuple_expression
        (float_literal)
        (float_literal))))
  (var_definition
    (let)
    (ignored_ident)
    (match_exp
      (match)
      (ident)
      (match_branch
        (tag_destruct
          (tag_ident))
        (float_literal))
      (match_branch
        (tag_destruct
          (tag_ident)
          (tuple_destruct
            (ident)
            (ident)))
        (ident)))))

